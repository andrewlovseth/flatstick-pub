<?php

/*

	Template Name: Book A Tee Time

*/

get_header(); ?>

	<section class="main">
		<div class="wrapper">

			<div class="header">
				<h2>Book A Tee Time</h2>
			</div>

			<div class="links tabs">
				<h4>Select a location</h4>

				<?php if(have_rows('location_links', 'options')): while(have_rows('location_links', 'options')): the_row(); ?>
					<?php $post_object = get_sub_field('location'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

						<?php if(get_field('hide_tee_time') != "true"): ?>
							<a href="<?php the_field('tee_time_link'); ?>" class="tab <?php the_field('abbreviation'); ?>" rel="external"><?php the_field('abbreviation'); ?></a>
						<?php endif; ?>

					<?php wp_reset_postdata(); endif; ?>
				<?php endwhile; endif; ?>
			</div>
			
		</div>
	</section>

<?php get_footer(); ?>