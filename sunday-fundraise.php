<?php

/*

	Template Name: Sunday Fundraise

*/

get_header(); ?>

	<section id="main">
		<div class="wrapper">

			<div class="headline">
				<h1 class="page-title"><?php the_title(); ?></h1>
			</div>

			<div class="intro copy">
				<?php the_field('intro_copy'); ?>
			</div>

			<div class="total">
				<div class="graphic">
					<img src="<?php $image = get_field('total_graphic'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="details">
					<h3>
						<?php the_field('total_headline'); ?><br/>
						<span class="amount"><?php the_field('total_amount'); ?></span>
					</h3>
				</div>
			</div>

		</div>
	</section>

	<section class="charities">
		<div class="wrapper">
			
			<div class="section-header">
				<h2><?php the_field('charities_headline'); ?></h2>

				<div class="copy p3">
					<?php the_field('charities_copy'); ?>
				</div>
			</div>

			<div class="grid">
				<?php if(have_rows('charities')): while(have_rows('charities')): the_row(); ?>
				 
				    <div class="item">
				    	<div class="location-header">
				    		<h5><?php the_sub_field('location'); ?></h5>
				    	</div>

				        <a href="<?php the_sub_field('link'); ?>" rel="external">
				        	<img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				        </a>
				    </div>

				<?php endwhile; endif; ?>				
			</div>

		</div>
	</section>

	<section class="partner">
		<div class="wrapper">
			
			<div class="section-header">
				<h2><?php the_field('partner_headline'); ?></h2>
			</div>

			<div class="photo">
				<img src="<?php $image = get_field('partner_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="details copy p2">
				<?php the_field('partner_copy'); ?>
			</div>

			<div class="note copy p3">
				<?php the_field('partner_note'); ?>
			</div>

			<div class="partner-form">
				<?php
					$shortcode = get_field('partner_form_shortcode');
					echo do_shortcode($shortcode);
				?>	
			</div>

		</div>
	</section>

<?php get_footer(); ?>