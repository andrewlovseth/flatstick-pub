<?php get_header(); ?>

	<section class="generic">
		<div class="wrapper">

			<section class="page-header">
				<h1><?php the_title(); ?></h1>	
			</section>

			<section class="page-body">
				<?php the_content(); ?>
			</section>

		</div>
	</section>

<?php get_footer(); ?>