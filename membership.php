<?php

/*

	Template Name: Membership

*/

get_header(); ?>

	<section id="main">
		<div class="wrapper">
			

			<section class="membership flatstick-membership">
				<div class="membership-header">
					<div class="headline">
						<h2><?php the_field('flatstick_membership_headline'); ?></h2>
					</div>					

					<div class="sub-headline">
						<h3><?php the_field('flatstick_membership_sub_headline'); ?></h3>
					</div>

					<div class="photo">
						<div class="content">
							<img src="<?php $image = get_field('flatstick_membership_photo'); echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>
						
					</div>

					<div class="deck">
						<?php the_field('flatstick_membership_deck'); ?>
					</div>

					<div class="cost">
						<h4>Cost</h4>
						<h2><?php the_field('flatstick_membership_cost'); ?></h2>
					</div>
				</div>

				<div class="membership-details">
					<?php the_field('flatstick_membership_details'); ?>
				</div>


			</section>

			<section class="membership beer-club">
				<div class="membership-header">
					<div class="headline">
						<h2><?php the_field('beer_club_headline'); ?></h2>
					</div>					

					<div class="sub-headline">
						<h3><?php the_field('beer_club_sub_headline'); ?></h3>
					</div>

					<div class="photo">
						<div class="content">
							<img src="<?php $image = get_field('beer_club_photo'); echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>
					</div>

					<div class="deck">
						<?php the_field('beer_club_deck'); ?>
					</div>

					<div class="cost">
						<h4>Cost</h4>
						<h2><?php the_field('beer_club_cost'); ?></h2>
					</div>
				</div>

				<div class="membership-details">
					<?php the_field('beer_club_details'); ?>

					<div class="apps">
						<a href="<?php the_field('app_store', 'options'); ?>" class="app-store" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/app-store.svg" alt="App Store"></a>
						<a href="<?php the_field('google_play', 'options'); ?>" class="google-play" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/google-play.png" alt="Google Play"></a>	
					</div>
				</div>
			</section>



		</div>
	</section>



<?php get_footer(); ?>