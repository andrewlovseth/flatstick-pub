<?php if(get_field('show_banner')): ?>

    <?php if(get_field('show_extended_banner')): ?>

        <section id="banner" class="extended">
            <div class="wrapper">

                <div class="info">

                    <div class="headline">
                        <h3><?php the_field('extended_banner_headline'); ?></h3>
                        <h4><?php the_field('extended_banner_sub_headline'); ?></h4>
                    </div>            

                    <div class="copy">
                        <?php the_field('extended_banner_copy'); ?>
                    </div>

                    <div class="cta">
                        <?php 
                        $link = get_field('extended_banner_cta');
                        if( $link ): 
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                            <a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                        <?php endif; ?>
                    </div>
                    
                </div>



                <div class="notice">
                    <h5><?php the_field('banner_headline'); ?></h5>
                    <?php the_field('banner_copy'); ?>
                </div>
                
            </div>
        </section>

    <?php else: ?>

        <section id="banner">
            <div class="wrapper">

            	<div class="headline">
            		<h3><?php the_field('banner_headline'); ?></h3>
            	</div>            

                <div class="copy">
                	<?php the_field('banner_copy'); ?>
                </div>
                
            </div>
        </section>

    <?php endif; ?>

<?php endif; ?>