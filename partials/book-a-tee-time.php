<div id="book-a-tee-time" class="modal">
	<div class="overlay">

		<a href="#" class="close tee-time-close">
			<img src="<?php bloginfo('template_directory') ?>/images/close-icon.svg" alt="Close Icon">
		</a>

		<div class="content">
			<div class="wrapper">

				<div class="header">
					<h2>Book a Tee Time</h2>
				</div>


				<div class="links tabs">
					<h4>Select a location</h4>

					<?php if(have_rows('location_links', 'options')): while(have_rows('location_links', 'options')): the_row(); ?>
						<?php $post_object = get_sub_field('location'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

							<?php if(get_field('hide_tee_time') != "true"): ?>
								<a href="<?php the_field('tee_time_link'); ?>" class="tab <?php the_field('abbreviation'); ?>" rel="external"><?php the_field('abbreviation'); ?></a>
							<?php endif; ?>

						<?php wp_reset_postdata(); endif; ?>
					<?php endwhile; endif; ?>
				</div>

			</div>
		</div>
		
	</div>
</div>