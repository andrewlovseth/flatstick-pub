<section class="template-section left-aligned-text" id="<?php echo sanitize_title_with_dashes(get_sub_field('title')); ?>">
	<div class="wrapper">

		<div class="info">
			<?php if(get_sub_field('title')): ?>
				<div class="headline section-headline">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			<?php endif; ?>
			
			<?php if(get_sub_field('section_image')): ?>
				<div class="hero-image">
					<img src="<?php $image = get_sub_field('section_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
			<?php endif; ?>	

			<?php if(get_sub_field('copy')): ?>
				<div class="copy">
					<?php the_sub_field('copy'); ?>
				</div>
			<?php endif; ?>	
			
			<?php if(get_sub_field('cta_link')): ?>

		        <div class="cta<?php if(get_sub_field('cta_style') == 'secondary') { echo ' cta--secondary'; } ?>">
		        		<a href="<?php echo esc_attr(get_sub_field('cta_link')); ?>"
			        		<?php if(get_sub_field('cta_class')) { echo 'class=" ' . esc_attr(get_sub_field('cta_class')) . '"'; } ?> 
				        	data-location="<?php echo $abbr; ?>"
					    >
			        		<span><?php the_sub_field('cta_label'); ?></span>
		        		</a>
		        </div>

	        <?php endif; ?>	

		</div>

	</div>
</section>