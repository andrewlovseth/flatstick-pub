<section class="template-section weekly-happenings" id="<?php echo sanitize_title_with_dashes(get_sub_field('title')); ?>">
	<div class="wrapper">
		<h2 class="section-header"><?php the_sub_field('title'); ?></h2>

		<?php if(have_rows('events')): ?>
			<div class="events">

				<?php while(have_rows('events')): the_row(); ?>
				    <div class="event">
				    	<div class="time">
				    		<h4><?php the_sub_field('time'); ?></h4>
				    	</div>

				    	<div class="title">
				    		<h3><?php the_sub_field('title'); ?></h3>
				    	</div>
				    </div>
				<?php endwhile; ?>

			</div>
		<?php endif; ?>

		<?php if(have_rows('specials')): ?>
			<div class="specials">

				<?php while(have_rows('specials')): the_row(); ?>
			 
				    <div class="special">
				    	<div class="title">
				    		<h3><?php the_sub_field('title'); ?></h3>
				    	</div>

				    	<div class="time">
				    		<h4><?php the_sub_field('time'); ?></h4>
				    	</div>

				    	<div class="details">
				    		<p><?php the_sub_field('details'); ?></p>
				    	</div>
				    </div>

				<?php endwhile; ?>
				
			</div>
		<?php endif; ?>
		
	</div>
</section>