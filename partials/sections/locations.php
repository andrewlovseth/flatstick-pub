<section class="template-section locations" id="<?php echo sanitize_title_with_dashes(get_sub_field('title')); ?>">
	<div class="wrapper">
		
		<?php if(get_sub_field('title')): ?>
			<div class="headline section-headline">
				<h2><?php the_sub_field('title'); ?></h2>
			</div>
		<?php endif; ?>

		<div class="locations-wrapper">
			
			<?php if(have_rows('location_links', 'options')): while(have_rows('location_links', 'options')): the_row(); ?>
				<?php $post_object = get_sub_field('location'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
			
					<div class="location">
						<a href="<?php the_permalink(); ?>">
							<div class="info">
								<strong><?php the_field('abbreviation'); ?></strong>
								<em><?php the_title(); ?></em>
							</div>
							<img src="<?php $image = get_field('main_image'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
						</a>					
					</div>

				<?php wp_reset_postdata(); endif; ?>

			<?php endwhile; endif; ?>					
		
		</div>

	</div>
</section>