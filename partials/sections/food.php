<section class="template-section food" id="<?php echo sanitize_title_with_dashes(get_sub_field('title')); ?>">
	<div class="wrapper">

		<div class="info">
			<h2 class="section-header"><?php the_sub_field('title'); ?></h2>

			<?php the_sub_field('copy'); ?>

			<div class="hours">
				<h4>Hours</h4>
				<p><?php the_sub_field('hours'); ?></p>
			</div>

			<?php if(get_sub_field('menu')): ?>
				<div class="cta">
					<a href="<?php the_sub_field('menu'); ?>" rel="external"><span>View Menu</span></a>

					<?php if(get_sub_field('order_online')): ?>
						<a href="<?php the_sub_field('order_online'); ?>" rel="external"><span>Order Food Now</span></a>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div>

	</div> <!--.wrapper-->

	<div class="background-accent cover" style="background-image: url(<?php $image = get_sub_field('background_image'); echo $image['url']; ?>);">
		
	</div>
</section>