<?php
    $stream_opts = [
        "ssl" => [
            "verify_peer" => false,
            "verify_peer_name" => false,
        ]
    ];
	$json_feed = get_sub_field('json_feed');

	if($json_feed):
	$json_contents = file_get_contents($json_feed, false, stream_context_create($stream_opts));
    $json = json_decode($json_contents, true);

?>

	<section class="template-section tap-list" id="<?php echo sanitize_title_with_dashes(get_sub_field('title')); ?>">
		<div class="wrapper">

			<h2 class="section-header"><?php the_sub_field('title'); ?></h2>
		
			<?php foreach ($json as $beverage): ?>
				<?php
					$item_name = $beverage['MenuItemDisplayDetail']['DisplayName'];
					$producer_name = $beverage['MenuItemProductDetail']['FullProducerList'];
					$beverage_name = $beverage['MenuItemProductDetail']['BeverageNameWithVintage'];
					$beverage_style = $beverage['MenuItemProductDetail']['FullStyleName'];
					$beverage_color = $beverage['MenuItemProductDetail']['Beverage']['StyleColor'];
					$year = $beverage['MenuItemProductDetail']['Year'];
					$beverage_abv = $beverage['MenuItemProductDetail']['Beverage']['Abv'];
					$beverage_type = $beverage['MenuItemProductDetail']['BeverageType'];
					$producer_location = "";
					$producer_url = "";
					$beverage_ibu = '';

					switch($beverage_type) {
						case "Beer":
							$producer_location = $beverage['MenuItemProductDetail']['Beverage']['Brewery']['Location'];
							$producer_url = $beverage['MenuItemProductDetail']['Beverage']['Brewery']['BreweryUrl'];
							$logo_url = $beverage['MenuItemProductDetail']['Beverage']['Brewery']['LogoImageUrl'];
							$beverage_ibu = $beverage['MenuItemProductDetail']['Beverage']['Ibu'];

							break;
						case "Cider":
							$producer_location = $beverage['MenuItemProductDetail']['Beverage']['Cidery']['Location'];
							$producer_url = $beverage['MenuItemProductDetail']['Beverage']['Cidery']['CideryUrl'];
							$logo_url = $beverage['MenuItemProductDetail']['Beverage']['Cidery']['LogoImageUrl'];
							$beverage_ibu = $beverage['MenuItemProductDetail']['Beverage']['Ibu'];

							break;
						case "Mead":
							$producer_location = $beverage['MenuItemProductDetail']['Beverage']['Meadery']['Location'];
							$producer_url = $beverage['MenuItemProductDetail']['Beverage']['Meadery']['MeaderyUrl'];
							$logo_url = $beverage['MenuItemProductDetail']['Beverage']['Meadery']['LogoImageUrl'];
							$beverage_ibu = $beverage['MenuItemProductDetail']['Beverage']['Ibu'];

							break;
						case "Wine":
							$producer_location = $beverage['MenuItemProductDetail']['Beverage']['Winery']['Region'];
							$producer_url = $beverage['MenuItemProductDetail']['Beverage']['Winery']['WineryUrl'];
							$logo_url = $beverage['MenuItemProductDetail']['Beverage']['Winery']['LogoImageUrl'];

							break;
						case "Kombucha":
							$producer_location = $beverage['MenuItemProductDetail']['Beverage']['KombuchaMaker']['Location'];
							$producer_url = $beverage['MenuItemProductDetail']['Beverage']['KombuchaMaker']['Url'];
							$logo_url = $beverage['MenuItemProductDetail']['Beverage']['KombuchaMaker']['LogoImageUrl'];

							break;
						case "Soft Drink":
							$producer_location = $beverage['MenuItemProductDetail']['Beverage']['SoftDrinkMaker']['Location'];
							$producer_url = $beverage['MenuItemProductDetail']['Beverage']['SoftDrinkMaker']['ProducersUrl'];
							$logo_url = $beverage['MenuItemProductDetail']['Beverage']['SoftDrinkMaker']['LogoImageUrl'];

							break;

						case "Hard Seltzer":
							$producer_location = $beverage['MenuItemProductDetail']['Beverage']['HardSeltzerMaker']['Location'];
							$producer_url = $beverage['MenuItemProductDetail']['Beverage']['HardSeltzerMaker']['ProducersUrl'];
							$logo_url = $beverage['MenuItemProductDetail']['Beverage']['HardSeltzerMaker']['LogoImageUrl'];

							break;


						case "Custom":
							$producer_location = $beverage['MenuItemProductDetail']['Beverage']['BeverageProducer']['Location'];
							$producer_url = $beverage['MenuItemProductDetail']['Beverage']['BeverageProducer']['ProducersUrl'];
							$logo_url = $beverage['MenuItemProductDetail']['Beverage']['ResolvedLogoImageUrl'];

							break;

					}
					$date_put_on = $beverage['DatePutOn']; 
					$bottle_size = $beverage['MenuItemProductDetail']['Prices'][0]['Size'];   
					$bottle_price = $beverage['MenuItemProductDetail']['Prices'][0]['Price'];
					$beverage_ps = $beverage['MenuItemProductDetail']['Prices'][0]['DisplayName']; 
					$in_bottles = $beverage['MenuItemProductDetail']['AvailableInBottles'];
					$keg_size = $beverage['MenuItemProductDetail']['KegSize'];
					$oz_remaining = $beverage['MenuItemProductDetail']['EstimatedOzLeft'];
					$scale = 1.0; //


					//calculating percentage of keg remaining
					// Get Percentage out of 100
					if ( !empty($keg_size) ) { $percent = $oz_remaining  / $keg_size; } 
					else { $percent = 0; }

					// Limit to 100 percent (if more than the max is allowed)
					if ( $percent > 1 ) { $percent = 1; }     
					if ( $percent < 0 ) { $percent = .005; }     
					$percent_remaining = number_format($percent*100, 0);
					if ( $percent_remaining < 1 ) {$percent_remaining = "< 1";}
					
					if($percent_remaining <= 20 ) {
						$percent_left_color = '#db3e2d';
					} elseif ($percent_remaining >= 21 && $percent_remaining <= 40) {
						$percent_left_color = '#e0691d';
					} elseif ($percent_remaining >= 41 && $percent_remaining <= 60) {
						$percent_left_color = '#c0c128';
					} elseif ($percent_remaining >= 51 && $percent_remaining <= 80) {
						$percent_left_color = '#4bd839';
					} elseif ($percent_remaining >= 81 && $percent_remaining <= 100) {
						$percent_left_color = '#00ff4f';
					}
				?>
		
				<div class="item">
					<div class="tap">
						<strong><?php echo $item_name; ?></strong>
						
					</div>
					<div class="logo">
						<?php if($logo_url): ?>
							<img src="<?php echo $logo_url; ?>" alt="">
						<?php else: ?>
						<img src="<?php bloginfo('template_directory') ?>/images/blank-tap.png" alt="">

						<?php endif; ?>	    		
					</div>

					<div class="info">
						<div class="name">
							<h3 class="beer"><span class="brewer"><?php echo $producer_name; ?></span> <span class="beverage"><?php echo $beverage_name; ?></span></h3>
							<h4 class="style"><?php echo $beverage_style; ?></h4>

							<div class="details">
								<p>
									<?php if($producer_location != null): ?>
										<span class="location"><?php echo $producer_location; ?></span>
									<?php endif; ?>
									<?php if($beverage_abv != null): ?>
										<span class="abv"><?php echo $beverage_abv; ?>%</span>
									<?php endif; ?>
									<?php if($beverage_ibu != null): ?>
										<span class="ibu"><?php echo $beverage_ibu; ?> IBU</span></p>
									<?php endif; ?>
							</div>
						</div>
					</div>

					<div class="remaining">
						<div class="meter">
							<div class="amount" style="height: <?php echo $percent_remaining; ?>%; background-color: <?php echo $percent_left_color; ?>;">
							</div> 			
						</div>
					</div>
				</div>			
			<?php endforeach; ?>

			<div class="powered-by">
				<a href="http://www.digitalpour.com/" target="_blank">
					<img src="<?php bloginfo('template_directory') ?>/images/digital-pour.png" alt="Digital Pour">
				</a>
			</div>

		</div>
	</section>

<?php endif; ?>