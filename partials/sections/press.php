<section class="template-section press" id="<?php echo sanitize_title_with_dashes(get_sub_field('title')); ?>">
	<?php if(get_sub_field('title')): ?>
		<div class="headline section-headline">
			<h2><?php the_sub_field('title'); ?></h2>
		</div>
	<?php endif; ?>	

	<?php if(have_rows('listings')): while(have_rows('listings')): the_row(); ?>
	 
	    <div class="clip">
	    	<h4><?php the_sub_field('source'); ?></h4>
	        <h3><a href="<?php the_sub_field('link'); ?>" rel="external"><?php the_sub_field('title'); ?></a></h3>
	    </div>

	<?php endwhile; endif; ?>
</section>