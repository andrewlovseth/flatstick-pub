<section class="template-section group-events" id="<?php echo sanitize_title_with_dashes(get_sub_field('title')); ?>">
	<div class="wrapper">
		<h2 class="section-header"><?php the_sub_field('title'); ?></h2>

		<?php if(get_sub_field('copy')): ?>		
			<div class="info">
				<?php the_sub_field('copy'); ?>		
			</div>
		<?php endif; ?>

		<?php if(get_sub_field('brochure_link')): ?>
			<div class="brochure">
				<a href="<?php the_sub_field('brochure_link'); ?>" rel="external"><?php the_sub_field('brochure_link_label'); ?></a>
			</div>
		<?php endif; ?>

		<?php if(get_field('plan_an_event', 'options') == 'on'): ?>
			<?php if(get_field('plan_an_event')): ?>
				<div class="cta">
					<a class="event" href="<?php the_field('plan_an_event'); ?>" rel="external"><span><?php the_field('plan_cta_title'); ?></span></a>
				</div>
			<?php endif; ?>
		<?php endif; ?>		
	</div>
</section>