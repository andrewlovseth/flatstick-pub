<section class="template-section centered-text" id="<?php echo sanitize_title_with_dashes(get_sub_field('title')); ?>">
	<div class="wrapper">

		<div class="info">
			<?php if(get_sub_field('title')): ?>
				<div class="headline section-headline">
					<h2><?php the_sub_field('title'); ?></h2>
				</div>
			<?php endif; ?>	

			<?php the_sub_field('copy'); ?>
			
			<?php if(get_sub_field('cta_link')): ?>

		        <div class="cta<?php if(get_sub_field('cta_style') == 'secondary') { echo ' cta--secondary'; } ?>">
		        		<a href="<?php echo esc_attr(get_sub_field('cta_link')); ?>"
			        		<?php if(get_sub_field('cta_class')) { echo 'class=" ' . esc_attr(get_sub_field('cta_class')) . '"'; } ?> 
				        	data-location="<?php echo $abbr; ?>"
					    >
			        		<span><?php the_sub_field('cta_label'); ?></span>
		        		</a>
		        </div>

	        <?php endif; ?>	

		</div>

	</div>
</section>