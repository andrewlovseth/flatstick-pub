<section class="template-section content-rows" id="<?php echo sanitize_title_with_dashes(get_sub_field('title')); ?>">
	<div class="wrapper">
			
		<?php if(get_sub_field('title')): ?>
			<div class="headline section-headline">
				<h2><?php the_sub_field('title'); ?></h2>
			</div>
		<?php endif; ?>	
		
		<?php if(have_rows('items')): while(have_rows('items')) : the_row(); ?>
		
				<div class="game">
					<div class="info">
						<div class="headline">
							<h3><?php the_sub_field('title'); ?></h3>
						</div>
						<div class="copy">
							<?php the_sub_field('description'); ?>
						</div>
						<?php if(get_sub_field('cta_link')): ?>

					        <div class="cta<?php if(get_sub_field('cta_style') == 'secondary') { echo ' cta--secondary'; } ?>">
					        		<a href="<?php echo esc_attr(get_sub_field('cta_link')); ?>"
						        		<?php if(get_sub_field('cta_class')) { echo 'class=" ' . esc_attr(get_sub_field('cta_class')) . '"'; } ?> 
							        	data-location="<?php echo $abbr; ?>"
								    >
						        		<span><?php the_sub_field('cta_label'); ?></span>
					        		</a>
					        </div>
	
				        <?php endif; ?>	
					</div>
	
					<div class="photo">
						<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
		    		
				</div>
		 
		<?php endwhile; endif; ?>
	
	</div>
</section>