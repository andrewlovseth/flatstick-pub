<div class="header-contact">
	<h2>
		<?php if(get_field('coming_soon')): ?>
			<span class="coming-soon"></span>
		<?php endif; ?>
		
		<?php
			if(get_field('contact_title')):
				the_field('contact_title');
			else:
				the_title();
			endif;
		?>
	</h2>

	<div class="info">
		<?php if(get_field('address')): ?><p class="address"><?php the_field('address'); ?></p><?php endif; ?>
		<?php if(get_field('phone')): ?>
			<p class="phone-hours">
				<?php the_field('phone'); ?><?php if(get_field('hours')): ?> / <a href="#contact" class="smooth">See Hours</a><?php endif; ?>
			</p>
		<?php endif; ?>
	</div>

	<div class="utility-nav">
		<?php if(get_field('book_a_tee_time', 'options') == 'on'): ?>
			<?php if(get_field('tee_time_link')): ?>
				<a href="<?php the_field('tee_time_link'); ?>" rel="external"><span><?php the_field('tee_time_cta_title'); ?></span></a>
			<?php endif; ?>
		<?php endif; ?>

		<?php if(get_field('plan_an_event', 'options') == 'on'): ?>
			<?php if(get_field('plan_an_event')): ?>
				<a href="<?php the_field('plan_an_event'); ?>" rel="external"><span><?php the_field('plan_cta_title'); ?></span></a>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</div>