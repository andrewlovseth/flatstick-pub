<section class="template-section column-row" id="<?php echo sanitize_title_with_dashes(get_sub_field('title')); ?>">
	<div class="wrapper">
		
		<?php $abbr = get_field('abbreviation'); ?>

		<?php if(have_rows('items')): while(have_rows('items')): the_row(); ?>
 
		    <div class="item">
		    	<div class="info">
			        <h2 class="section-header"><?php the_sub_field('title'); ?></h2>
			        <?php the_sub_field('copy'); ?>
			        <h3><?php the_sub_field('price'); ?></h3>

			        <?php if(get_sub_field('cta_link')): ?>

					        <div class="cta<?php if(get_sub_field('cta_style') == 'secondary') { echo ' cta--secondary'; } ?>">
					        		<a href="<?php echo esc_attr(get_sub_field('cta_link')); ?>"
						        		<?php if(get_sub_field('cta_class')) { echo 'class=" ' . esc_attr(get_sub_field('cta_class')) . '"'; } ?> 
							        	data-location="<?php echo $abbr; ?>"
								    >
						        		<span><?php the_sub_field('cta_label'); ?></span>
					        		</a>
					        </div>
	
				        <?php endif; ?>		    		
		    	</div>

		    	<div class="image">
		    		<img src="<?php $image = get_sub_field('image'); echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
		    	</div>
		    </div>

		<?php endwhile; endif; ?>

	</div>
</section>