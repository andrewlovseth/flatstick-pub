<div id="plan-an-event" class="modal">
	<div class="overlay">

		<a href="#" class="close event-close">
			<img src="<?php bloginfo('template_directory') ?>/images/close-icon.svg" alt="Close Icon">
		</a>

		<div class="content">
			<div class="wrapper">

				<div class="header">
					<h2>Plan an Event</h2>
				</div>

				<div class="tabs event-tabs">
					<h4>Select a location</h4>
					
					<?php if(have_rows('location_links', 'options')): while(have_rows('location_links', 'options')): the_row(); ?>
						<?php $post_object = get_sub_field('location'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

							<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>
							    <?php if( get_row_layout() == 'group_events' ): ?>
									<?php $brochure = get_sub_field('brochure_link');; ?>
							    <?php endif; ?>
							<?php endwhile; endif; ?>

							<?php if(get_field('plan_an_event')): ?>	
								<a href="<?php the_permalink(); ?>" class="tab <?php the_field('abbreviation'); ?>" data-form="https://gatherhere.com/inquiries/<?php the_field('plan_an_event'); ?>" data-brochure="<?php echo $brochure; ?>" data-location="<?php the_field('abbreviation'); ?>">
									<?php the_field('abbreviation'); ?>
								</a>
							<?php endif; ?>

						<?php wp_reset_postdata(); endif; ?>
					<?php endwhile; endif; ?>		
				</div>

				<div id="event-info">
					<div class="copy">
						<p><?php the_field('plan_an_event_copy', 'options'); ?></p>
					</div>

					<div class="cta">
						<a href="#" rel="external">
							<span class="link-wrapper">
								<span class="location"></span> Events Brochure
							</span>
						</a>
					</div>
				</div>


				<div class="form event-form">
					<h3 class="location-name"></h3>
					<div class="form-wrapper">
						<iframe frameborder="0" scrolling="no" src="" title="Booking Inquiry Form"></iframe>
					</div>
				</div>

			</div>
		</div>
		
	</div>
</div>