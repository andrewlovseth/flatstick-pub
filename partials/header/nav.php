<nav class="site-nav">
	<div class="nav-wrapper">

		<div class="info">
			<div class="logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('nav_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<div class="links">
				<?php if(have_rows('nav_link', 'options')): while(have_rows('nav_link', 'options')): the_row(); ?>

				    <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>

				<?php endwhile; endif; ?>					
			</div>

			<div class="locations">
				<h5>Locations</h5>

				<div class="locations-grid">
					<?php if(have_rows('location_links', 'options')): while(have_rows('location_links', 'options')): the_row(); ?>

						<?php $post_object = get_sub_field('location'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
							<a href="<?php the_permalink(); ?>">
								<span class="abbr"><?php the_field('abbreviation'); ?></span>
								<?php if(get_field('coming_soon')): ?>
									<span class="coming-soon"></span>
								<?php endif; ?>								
							</a>
						<?php wp_reset_postdata(); endif; ?>

					<?php endwhile; endif; ?>	
				</div>
			</div>

			<div class="utility-nav">
				<?php if(get_field('book_a_tee_time', 'options') == 'on'): ?>
					<a href="<?php echo site_url('/book-tee-time/'); ?>"><span>Book a Tee Time</span></a>
				<?php endif; ?>

				<?php if(get_field('plan_an_event', 'options') == 'on'): ?>
					<a href="<?php echo site_url('/plan-an-event/'); ?>"><span>Plan an Event</span></a>
				<?php endif; ?>
				
				<a href="https://www.clover.com/online-ordering/flatstick-pub--kirkland-kirkland/giftcard " rel="external"><span>Buy a Gift Card</span></a>
			</div>

			<div class="social-nav">
				<h5>Social</h5>

				<a href="<?php the_field('facebook', 'options'); ?>" class="facebook" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/facebook.svg" alt="Facebook"></a>
				<a href="<?php the_field('twitter', 'options'); ?>" class="twitter" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/twitter.svg" alt="Twitter"></a>
				<a href="<?php the_field('instagram', 'options'); ?>" class="instagram" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/instagram.svg" alt="Instagram"></a>
				<a href="<?php the_field('foursquare', 'options'); ?>" class="foursquare" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/foursquare.svg" alt="Foursquare"></a>
			</div>
		</div>
		
	</div>
</nav>