<div class="logo">
	<a href="<?php echo site_url('/'); ?>">
		<?php if(is_page_template('homepage.php')): ?>
			<img src="<?php $image = get_field('home_logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		<?php else: ?>
			<img src="<?php $image = get_field('header_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		<?php endif; ?>
	</a>
</div>