<section class="press grid">
    <div class="headline">
        <h2 class="section-header"><?php the_field('press_headline'); ?></h2>
    </div>

    <?php if(have_rows('press')): while(have_rows('press')): the_row(); ?>
 
        <div class="clip">
	    	<h4><?php the_sub_field('source'); ?></h4>
            <?php 
                $link = get_sub_field('link');
                if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
            ?>
                <h3><a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a></h3>
            <?php endif; ?>
	    </div>
    
    <?php endwhile; endif; ?>
</section>