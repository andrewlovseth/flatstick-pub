<section class="games grid">
    <div class="headline">
        <h2 class="section-header"><?php the_field('games_headline'); ?></h2>
    </div>

    <?php if(have_rows('games')): while(have_rows('games')): the_row(); ?>
 
        <div class="game">
            <div class="info">
                <div class="headline">
                    <h3><?php the_sub_field('headline'); ?></h3>
                </div>
                <div class="copy">
                    <?php the_sub_field('copy'); ?>
                </div>
            </div>
	
            <div class="photo">
                <img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </div>
        </div>
    
    <?php endwhile; endif; ?>
</section>