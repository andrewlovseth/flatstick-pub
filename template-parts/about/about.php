<?php
    $about = get_field('about');
    $headline = $about['headline'];
    $photo = $about['photo'];
    $copy = $about['copy'];
?>

<section class="about grid">
    <div class="headline">
        <h1 class="section-header"><?php echo $headline; ?></h1>
    </div>

    <div class="photo">
        <img src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['alt']; ?>" />
    </div>

    <div class="copy">
        <?php echo $copy; ?>
    </div>
</section>