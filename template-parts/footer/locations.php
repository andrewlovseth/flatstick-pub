<div class="locations">
    <?php if(have_rows('location_links', 'options')): while(have_rows('location_links', 'options')): the_row(); ?>
        <?php $post_object = get_sub_field('location'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
            <a href="<?php the_permalink(); ?>">
                <span class="abbr"><?php the_field('abbreviation'); ?></span>
                <?php if(get_field('coming_soon')): ?>
                    <span class="coming-soon"></span>
                <?php endif; ?>
            </a>
        <?php wp_reset_postdata(); endif; ?>
    <?php endwhile; endif; ?>		
</div>