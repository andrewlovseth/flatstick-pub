<div class="social-nav">
	<a href="<?php the_field('facebook', 'options'); ?>" class="facebook" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/facebook.svg" alt="Facebook"></a>
	<a href="<?php the_field('twitter', 'options'); ?>" class="twitter" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/twitter.svg" alt="Twitter"></a>
	<a href="<?php the_field('instagram', 'options'); ?>" class="instagram" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/instagram.svg" alt="Instagram"></a>
	<a href="<?php the_field('foursquare', 'options'); ?>" class="foursquare" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/foursquare.svg" alt="Foursquare"></a>

	<div class="apps">
		<a href="<?php the_field('app_store', 'options'); ?>" class="app-store" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/app-store.svg" alt="App Store"></a>
		<a href="<?php the_field('google_play', 'options'); ?>" class="google-play" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/google-play.png" alt="Google Play"></a>	
	</div>

</div>