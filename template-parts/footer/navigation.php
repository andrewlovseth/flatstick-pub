<div class="links">
    <?php if(have_rows('nav_link', 'options')): while(have_rows('nav_link', 'options')): the_row(); ?>

        <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>

    <?php endwhile; endif; ?>					
</div>