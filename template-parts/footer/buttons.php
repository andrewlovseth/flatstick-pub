<div class="utility-nav">  
    <?php if(get_field('book_a_tee_time', 'options') == 'on'): ?>
        <a href="<?php echo site_url('/book-tee-time/'); ?>"><span>Book a Tee Time</span></a>
    <?php endif; ?>

    <?php if(get_field('plan_an_event', 'options') == 'on'): ?>
        <a href="<?php echo site_url('/plan-an-event/'); ?>"><span>Plan an Event</span></a>
    <?php endif; ?>



    <a href="https://www.clover.com/online-ordering/flatstick-pub--kirkland-kirkland/giftcard " rel="external"><span>Buy a Gift Card</span></a>
</div>