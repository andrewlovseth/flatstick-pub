<section class="about grid">

    <div class="info">
        <h3><?php the_field('about_tagline'); ?></h3>
        <?php the_field('about_copy'); ?>				
    </div>

</section>