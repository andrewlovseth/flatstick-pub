<section class="video-hero">
    <div class="content">

        <div class="video-container">
            <?php the_field('video_hero_embed'); ?>
        </div>

        <a href="#locations" class="down smooth">
            <img src="<?php bloginfo('template_directory') ?>/images/down-arrow.svg" alt="Down Arrow">
        </a>

    </div>
</section>