<section class="locations grid" id="locations">
        
    <h2 class="section-header">Locations</h2>

    <div class="locations-grid">
        
        <?php if(have_rows('location_links', 'options')): while(have_rows('location_links', 'options')): the_row(); ?>
            <?php $post_object = get_sub_field('location'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
        
                <div class="location">
                    <a href="<?php the_permalink(); ?>">
                        <div class="info">
                            <strong><?php the_field('abbreviation'); ?></strong>
                            <em><?php the_title(); ?></em>
                            <?php if(get_field('coming_soon')): ?>
                                <div class="coming-soon">
                                    <h5><span class="text"><?php the_field('coming_soon_text'); ?></span></h5>
                                </div>
                            <?php endif; ?>
                        </div>
                        <img src="<?php $image = get_field('main_image'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </a>					
                </div>

            <?php wp_reset_postdata(); endif; ?>

        <?php endwhile; endif; ?>					
    
    </div>

</section>