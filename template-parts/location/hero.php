<?php if(get_field('main_image')): ?>

    <section id="hero">
        <div class="main-image cover<?php if ( get_field('main_image_shorter') ) echo ' main-image--shorter'; ?>" style="background-image: url(<?php $image = get_field('main_image'); echo $image['sizes']['large']; ?>);">
            <div class="info">
                <div class="wrapper cover-headline">
                    <h1>
                        <?php
                            if(get_field('contact_title')):
                                the_field('contact_title');
                            else:
                                the_title();
                            endif;
                        ?>
                    </h1>		
                </div>
            </div>
        </div>

        <?php $images = get_field('image_gallery'); if( $images ): ?>
            <div class="gallery">
                <?php foreach( $images as $image ): ?>
                    <div class="image">
                        <div class="content">
                            <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>				

    </section>

    <?php else: ?>

    <section id="hero">
        <div class="headline">
            <h1 <?php if ( get_field('page_display_title') == false ) echo 'class="screen-reader-text"'; ?>><?php the_title(); ?></h1>
        </div>
    </section>

<?php endif; ?>