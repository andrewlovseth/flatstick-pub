<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>
 
 <?php if( get_row_layout() == 'weekly_happenings' ): ?>
     
     <?php get_template_part('partials/sections/weekly-happenings'); ?>
     
 <?php endif; ?>

 <?php if( get_row_layout() == 'columns_row' ): ?>
 
     <?php get_template_part('partials/sections/column-row'); ?>
     
 <?php endif; ?>
 
 <?php if( get_row_layout() == 'content_rows' ): ?>
 
     <?php get_template_part('partials/sections/content-rows'); ?>
     
 <?php endif; ?>

 <?php if( get_row_layout() == 'tap_list' ): ?>
     
     <?php get_template_part('partials/sections/tap-list'); ?>
     
 <?php endif; ?>

 <?php if( get_row_layout() == 'food' ): ?>
     
     <?php get_template_part('partials/sections/food'); ?>
     
 <?php endif; ?>
 
 <?php if( get_row_layout() == 'press' ): ?>
     
     <?php get_template_part('partials/sections/press'); ?>
     
 <?php endif; ?>
 
 <?php if( get_row_layout() == 'locations' ): ?>
     
     <?php get_template_part('partials/sections/locations'); ?>
     
 <?php endif; ?>

 <?php if( get_row_layout() == 'group_events' ): ?>
     
     <?php get_template_part('partials/sections/group-events'); ?>
     
 <?php endif; ?>

 <?php if( get_row_layout() == 'centered_text' ): ?>
     
     <?php get_template_part('partials/sections/centered-text'); ?>
     
 <?php endif; ?>
 
  <?php if( get_row_layout() == 'left_aligned_text' ): ?>
     
     <?php get_template_part('partials/sections/left-aligned-text'); ?>
     
 <?php endif; ?>

<?php endwhile; endif; ?>