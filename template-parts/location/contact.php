<?php if(get_field('address')): ?>
	
    <section class="template-section" id="contact">
        <div class="wrapper">
            
            <h2 class="section-header">Contact</h2>

            <div class="info">
                <div class="detail">
                    <h3>Address</h3>
                    <p class="address"><?php the_field('address'); ?></p>	
                </div>

                <?php if(get_field('phone')): ?>	
                    <div class="detail">
                        <h3>Phone</h3>
                        <p class="phone"><?php the_field('phone'); ?></p>	
                    </div>
                <?php endif; ?>

                <?php if(get_field('email')): ?>	
                    <div class="detail">
                        <h3>Email</h3>
                        <p class="email"><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p>	
                    </div>
                <?php endif; ?>

                <?php if(get_field('hours')): ?>	
                    <div class="detail">
                        <h3>Hours</h3>
                        <p class="hours"><?php the_field('hours'); ?></p>	
                    </div>
                <?php endif; ?>
            </div>

            <?php if(get_field('map')): ?>
                <div class="map">
                    <?php the_field('map'); ?>
                </div>
            <?php endif; ?>

        </div>
    </section>

<?php endif; ?>