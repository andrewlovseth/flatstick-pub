<?php if(get_field('display_page_navigation') == true): ?>
    <section id="location-nav">
        <div class="wrapper">

            <div class="links">
                <?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>
                    <?php if( get_row_layout() == 'weekly_happenings' ): ?><?php get_template_part('partials/sections/section-nav'); ?><?php endif; ?>
                    <?php if( get_row_layout() == 'columns_row' ): ?><?php get_template_part('partials/sections/section-nav'); ?><?php endif; ?>
                    <?php if( get_row_layout() == 'tap_list' ): ?>
                        <?php $json_feed = get_sub_field('json_feed'); if($json_feed): ?>
                            <?php get_template_part('partials/sections/section-nav'); ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if( get_row_layout() == 'food' ): ?><?php get_template_part('partials/sections/section-nav'); ?><?php endif; ?>
                    <?php if( get_row_layout() == 'group_events' ): ?><?php // get_template_part('partials/sections/section-nav'); ?><?php endif; ?>
                <?php endwhile; endif; ?>
            </div>
            
        </div>
    </section>
<?php endif; ?>