<?php

/*

	Template Name: Plan an Event

*/

get_header(); ?>

	<section class="main">

		<div class="header">
			<h2>Plan an Event</h2>
		</div>

		<div class="tabs event-tabs">
			<h4>Locations</h4>
			
			<?php if(have_rows('location_links', 'options')): while(have_rows('location_links', 'options')): the_row(); ?>
				<?php $post_object = get_sub_field('location'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

					<?php if(get_field('plan_an_event')): ?>
						<a href="<?php the_field('plan_an_event'); ?>" rel="external"><?php the_field('abbreviation'); ?></a>
					<?php endif; ?>

				<?php wp_reset_postdata(); endif; ?>
			<?php endwhile; endif; ?>		
		</div>

		<div id="notice">
			<h3>We are excited you are interested in hosting an event at Flatstick Pub!<br />
			Once you submit your event request, expect to hear back from us within 48 hours.</h3>
			<br />
			<p>Please make sure to check your junk/spam folders if you do not see a response within that time frame. We look forward to working with you!</p>
		</div>


		
	</section>

<?php get_footer(); ?>