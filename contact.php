<?php

/*

	Template Name: Contact

*/

get_header(); ?>


	<section id="main">
		<div class="wrapper">
			
			<h1 class="page-title">Contact</h1>

			<div class="locations">
		
				<?php if(have_rows('location_links', 'options')): while(have_rows('location_links', 'options')): the_row(); ?>
					<?php $post_object = get_sub_field('location'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
						
						
						<div class="location">
							<h2><a href="<?php the_permalink(); ?>"><?php the_field('abbreviation'); ?></a></h2>

							<div class="image">
								<div class="content">
									<img src="<?php $image = get_field('main_image'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
								</div>								
							</div>

							<div class="info">
								<?php if(get_field('address')): ?>	
									<div class="detail">
										<h3>Address</h3>
										<p class="address"><?php the_field('address'); ?></p>	
									</div>
								<?php endif; ?>

								
								<?php if(get_field('phone')): ?>	
									<div class="detail">
										<h3>Phone</h3>
										<p class="phone"><?php the_field('phone'); ?></p>	
									</div>
								<?php endif; ?>

								<?php if(get_field('email')): ?>	
									<div class="detail">
										<h3>Email</h3>
										<p class="email"><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p>	
									</div>
								<?php endif; ?>

								<?php if(get_field('hours')): ?>	
									<div class="detail">
										<h3>Hours</h3>
										<p class="hours"><?php the_field('hours'); ?></p>	
									</div>
								<?php endif; ?>


								<?php if(get_field('parking')): ?>	
									<div class="detail">
										<h3>Parking</h3>
										<?php the_field('parking'); ?>
									</div>
								<?php endif; ?>
							</div>
						</div>


					<?php wp_reset_postdata(); endif; ?>
				<?php endwhile; endif; ?>	

			</div>

		</div>
	</section>

<?php get_footer(); ?>