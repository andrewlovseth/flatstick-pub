<?php

/*

	Template Name: FAQs

*/

get_header(); ?>
	
	<section id="main">
		<div class="wrapper">

			<h1 class="page-title">FAQs</h1>

			<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>

				<section class="question-group">

				    <?php if( get_row_layout() == 'faqs' ): ?>

					    <?php if(have_rows('questions')): while(have_rows('questions')): the_row(); ?>
	 
							<div class="question">
								<h3><?php the_sub_field('question'); ?></h3>
					    		<?php the_sub_field('answer'); ?>
							</div>
												
						<?php endwhile; endif; ?>
						
				    <?php endif; ?>
					
				</section>
			 
			<?php endwhile; endif; ?>

		</div>
	</section>

<?php get_footer(); ?>