<?php

/*

	Template Name: Upcoming Events

*/

get_header(); ?>


	<section id="main">
		<div class="wrapper">

			<div class="headline">
				<h1 class="page-title"><?php the_title(); ?></h1>
			</div>


			<div class="events">
				
				<?php if(have_rows('events')): while(have_rows('events')) : the_row(); ?>
				 
				    <?php if( get_row_layout() == 'event' ): ?>
						
						<div class="event">
							<div class="image">
								<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>

							<div class="info">
								<h4 class="date"><?php the_sub_field('date_time'); ?></h4>
								<h3><?php the_sub_field('title'); ?></h3>
								<h4 class="location"><?php the_sub_field('location'); ?></h4>

								<div class="description">
									<?php the_sub_field('description'); ?>
								</div>

								<?php if(get_sub_field('cta_link')): ?>
									<div class="cta">
										<a href="<?php the_sub_field('cta_link'); ?>" rel="external"><?php the_sub_field('cta_label'); ?></a>
									</div>
								<?php endif; ?>
							</div>
						</div>
						
				    <?php endif; ?>
				 
				<?php endwhile; endif; ?>



			</div>

		</div>
	</section>

<?php get_footer(); ?>