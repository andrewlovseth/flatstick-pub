<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

	<?php get_template_part('partials/banner'); ?>

	<?php get_template_part('partials/header/nav'); ?>

	<div id="page">
	
		<header class="site-header">
			<div class="wrapper">

				<?php get_template_part('partials/header/logo'); ?>

				<?php get_template_part('template-parts/location/header-contact'); ?>

				<?php get_template_part('partials/header/toggle'); ?>

			</div>
		</header>