<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<main class="site-content">
		<?php get_template_part('template-parts/home/video-hero'); ?>

		<?php get_template_part('template-parts/home/about'); ?>

		<?php get_template_part('template-parts/home/locations'); ?>
	</main>

<?php get_footer(); ?>