(function ($, window, document, undefined) {

	$(document).ready(function() {

		// rel="external"
		$('a[rel="external"]').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});

		// Menu Toggle
		$('#toggle').click(function(){
			$('.site-header, .site-nav, #page').toggleClass('open');
			return false;
		});

		$('.video-container').fitVids();

		// Header, Nav, #page Toggle
		$('#page').on('click', function(){
			$('site-header, .site-nav, #page').removeClass('open');
		});

		// Smooth Scroll
		$('.smooth').smoothScroll();


		var timeoutId;
		var $videoBgAspect = $(".videobg-aspect");
		var $videoBgWidth = $(".videobg-width");
		var videoAspect = $videoBgAspect.outerHeight() / $videoBgAspect.outerWidth();

		function videobgEnlarge() {
		windowAspect = ($(window).height() / $(window).width());
		if (windowAspect > videoAspect) {
			$videoBgWidth.width((windowAspect / videoAspect) * 100 + '%');
		} else {
			$videoBgWidth.width(100 + "%")
		}
		}

		$(window).resize(function() {
			clearTimeout(timeoutId);
			timeoutId = setTimeout(videobgEnlarge, 100);
		});

		$(function() {
			videobgEnlarge();
		});


		// Location Carousel
		$('body.page-template-location #hero .gallery').slick({
			dots: true,
			arrows: false,
			infinite: true,
			speed:  500,
			autoplay: true,
			autoplaySpeed: 4000,
			centerMode: true,
			mobileFirst: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			responsive: [{
				breakpoint: 568,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			}]
		});
		
	});

	$(document).keyup(function(e) {
		
		if (e.keyCode == 27) {
			$('.site-header, .site-nav, #page').removeClass('open');
		}

	});

})(jQuery, window, document);