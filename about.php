<?php

/*

	Template Name: About

*/

get_header(); ?>

	<main class="site-content">
		<?php get_template_part('template-parts/about/about'); ?>

		<?php get_template_part('template-parts/about/games'); ?>

		<?php get_template_part('template-parts/about/press'); ?>
	</main>

<?php get_footer(); ?>