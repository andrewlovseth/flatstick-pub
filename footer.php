		<footer class="site-footer grid">
			<div class="info">
				<?php get_template_part('template-parts/footer/logo'); ?>

				<?php get_template_part('template-parts/footer/navigation'); ?>

				<div class="meta-links">
					<?php get_template_part('template-parts/footer/locations'); ?>

					<?php get_template_part('template-parts/footer/buttons'); ?>
				</div>

				<?php get_template_part('template-parts/footer/social-nav'); ?>
			</div>
		</footer>

		<?php wp_footer(); ?>

	</div><!--#page-->

</body>
</html>