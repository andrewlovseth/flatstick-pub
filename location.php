<?php

/*

	Template Name: Location

*/

get_header(); ?>

	<main class="site-content <?php echo $post->post_name; ?>">
		<?php get_template_part('template-parts/location/mobile-header-contact'); ?>

		<?php get_template_part('template-parts/location/hero'); ?>

		<?php get_template_part('template-parts/location/navigation'); ?>

		<?php get_template_part('template-parts/location/sections'); ?>

		<?php get_template_part('template-parts/location/contact'); ?>
	</main>

<?php get_footer(); ?>