<?php

/*

	Template Name: General

*/

get_header(); ?>


	<div class="mobile-header-contact">
		<div class="wrapper">
			
			<h2>
				<?php if(get_field('coming_soon')): ?>
					<span class="coming-soon"></span>
				<?php endif; ?>

				<?php
					if(get_field('contact_title')):
						the_field('contact_title');
					else:
						the_title();
					endif;
				?>
			</h2>

			<div class="info">
				<?php if(get_field('address')): ?><p class="address"><?php the_field('address'); ?></p><?php endif; ?>
				<?php if(get_field('phone')): ?><p class="phone-hours"><?php the_field('phone'); ?><?php if(get_field('hours')): ?>	 / <a href="#contact" class="smooth">See Hours</a><?php endif; ?></p><?php endif; ?>
			</div>

			<div class="utility-nav">
				<?php if(get_field('book_a_tee_time', 'options') == 'on'): ?>
					<a href="<?php the_field('tee_time_link'); ?>" rel="external"><span><?php the_field('tee_time_cta_title'); ?></span></a>
				<?php endif; ?>
				<?php if(get_field('plan_cta_title')): ?>
					<!--<a href="#" class="event-trigger" data-location="<?php echo $abbr; ?>"><span><?php the_field('plan_cta_title'); ?></span></a>-->
				<?php endif; ?>
			</div>

		</div>
	</div>



	<?php if(get_field('main_image')): ?>

		<section id="hero">
			<div class="main-image cover<?php if ( get_field('main_image_shorter') ) echo ' main-image--shorter'; ?>" style="background-image: url(<?php $image = get_field('main_image'); echo $image['sizes']['large']; ?>);">
				<div class="info">
					<div class="wrapper cover-headline">
						<h1>
							<?php
								if(get_field('contact_title')):
									the_field('contact_title');
								else:
									the_title();
								endif;
							?>
						</h1>		
					</div>
				</div>
			</div>
	
			<?php $images = get_field('image_gallery'); if( $images ): ?>
				<div class="gallery">
					<?php foreach( $images as $image ): ?>
						<div class="image">
							<div class="content">
								<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>				
	
		</section>

	<?php else: ?>

		<section id="hero">
			<div class="headline">
				<h1 <?php if ( get_field('page_display_title') == false ) echo 'class="screen-reader-text"'; ?>><?php the_title(); ?></h1>
			</div>
		</section>

	<?php endif; ?>
	
	<?php if(get_field('display_page_navigation') == true): ?>
		<section id="location-nav">
			<div class="wrapper">

				<div class="links">
					<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>
						<?php if( get_row_layout() == 'weekly_happenings' ): ?><?php get_template_part('partials/sections/section-nav'); ?><?php endif; ?>
						<?php if( get_row_layout() == 'columns_row' ): ?><?php get_template_part('partials/sections/section-nav'); ?><?php endif; ?>
						<?php if( get_row_layout() == 'tap_list' ): ?>
							<?php $json_feed = get_sub_field('json_feed'); if($json_feed): ?>
								<?php get_template_part('partials/sections/section-nav'); ?>
							<?php endif; ?>
						<?php endif; ?>
						<?php if( get_row_layout() == 'food' ): ?><?php get_template_part('partials/sections/section-nav'); ?><?php endif; ?>
						<?php if( get_row_layout() == 'group_events' ): ?><?php // get_template_part('partials/sections/section-nav'); ?><?php endif; ?>
					<?php endwhile; endif; ?>
				</div>
				
			</div>
		</section>
	<?php endif; ?>

	<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>
 
	    <?php if( get_row_layout() == 'weekly_happenings' ): ?>
			
			<?php get_template_part('partials/sections/weekly-happenings'); ?>
			
	    <?php endif; ?>

	    <?php if( get_row_layout() == 'columns_row' ): ?>
	    
			<?php get_template_part('partials/sections/column-row'); ?>
			
	    <?php endif; ?>
	    
	    <?php if( get_row_layout() == 'content_rows' ): ?>
	    
			<?php get_template_part('partials/sections/content-rows'); ?>
			
	    <?php endif; ?>

	    <?php if( get_row_layout() == 'tap_list' ): ?>
			
			<?php get_template_part('partials/sections/tap-list'); ?>
			
	    <?php endif; ?>

	    <?php if( get_row_layout() == 'food' ): ?>
			
			<?php get_template_part('partials/sections/food'); ?>
			
	    <?php endif; ?>
	    
	    <?php if( get_row_layout() == 'press' ): ?>
			
			<?php get_template_part('partials/sections/press'); ?>
			
	    <?php endif; ?>
	    
	    <?php if( get_row_layout() == 'locations' ): ?>
			
			<?php get_template_part('partials/sections/locations'); ?>
			
	    <?php endif; ?>

	    <?php if( get_row_layout() == 'group_events' ): ?>
			
			<?php get_template_part('partials/sections/group-events'); ?>
			
	    <?php endif; ?>
	 
	    <?php if( get_row_layout() == 'centered_text' ): ?>
			
			<?php get_template_part('partials/sections/centered-text'); ?>
			
	    <?php endif; ?>
	    
	     <?php if( get_row_layout() == 'left_aligned_text' ): ?>
			
			<?php get_template_part('partials/sections/left-aligned-text'); ?>
			
	    <?php endif; ?>
	 
	<?php endwhile; endif; ?>

	<?php if(get_field('address')): ?>
	
		<section class="template-section" id="contact">
			<div class="wrapper">
				
				<h2 class="section-header">Contact</h2>

				<div class="info">
					<div class="detail">
						<h3>Address</h3>
						<p class="address"><?php the_field('address'); ?></p>	
					</div>

					<?php if(get_field('phone')): ?>	
						<div class="detail">
							<h3>Phone</h3>
							<p class="phone"><?php the_field('phone'); ?></p>	
						</div>
					<?php endif; ?>

					<?php if(get_field('email')): ?>	
						<div class="detail">
							<h3>Email</h3>
							<p class="email"><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p>	
						</div>
					<?php endif; ?>

					<?php if(get_field('hours')): ?>	
						<div class="detail">
							<h3>Hours</h3>
							<p class="hours"><?php the_field('hours'); ?></p>	
						</div>
					<?php endif; ?>
				</div>

				<?php if(get_field('map')): ?>
					<div class="map">
						<?php the_field('map'); ?>
					</div>
				<?php endif; ?>

			</div>
		</section>

	<?php endif; ?>

<?php get_footer(); ?>